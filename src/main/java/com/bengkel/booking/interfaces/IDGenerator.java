package com.bengkel.booking.interfaces;

public interface IDGenerator {
    void generateId(String customerId);
}
