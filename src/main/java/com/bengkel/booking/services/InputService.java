package com.bengkel.booking.services;

import com.bengkel.booking.models.Customer;
import com.bengkel.booking.models.ItemService;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class InputService {
    private final Scanner scanner;
    private final ValidationService validationService;

    public InputService(Scanner scanner, ValidationService validationService) {
        this.scanner = scanner;
        this.validationService = validationService;
    }

    public String getInputPassword() {
        System.out.println("Masukan password :");
        return scanner.nextLine();
    }

    public String getInputCustomerId() {
        System.out.println("Masukan customer id :");
        return scanner.nextLine();
    }

    public String getInputPaymentMethod() {
        System.out.println("Silahkan pilih metode pembayaran(Saldo Coin atau Cash)");
        return scanner.nextLine();
    }

    public List<ItemService> getInputListService(Customer customer, List<ItemService> availableServices) {
        List<ItemService> services = new ArrayList<>();
        while (true) {
            ItemService itemService = validationService.validasiItemService(scanner, availableServices);
            if (services.contains(itemService)) {
                System.out.println("Anda sudah memilih service ini");
                continue;
            }
            services.add(itemService);
            boolean isServiceQuotaAvailable = validationService.validasiServiceQuota(services, customer);
            if (isServiceQuotaAvailable) {
                System.out.println("Anda sudah menggunakan semua jatah service");
                break;
            }
            String response = "";
            while (true) {
                System.out.println("Apakah anda ingin menambahkan service lainnya ? (Y/T)");
                response = scanner.nextLine();
                if (!(response.equalsIgnoreCase("Y") || response.equalsIgnoreCase("T"))) {
                    System.out.println("Input hanya bisa Y / T");
                    continue;
                }
                break;
            }
            if (response.equalsIgnoreCase("T")) {
                break;
            }
        }
        return services;
    }

    public String getInputVehicleId() {
        System.out.println("Masukan Vehicle id : ");
        return scanner.nextLine();
    }

    public String getTopUpInput() {
        String balance = "";
        while (true) {
            System.out.println("Masukan besaran Top Up");
            balance = scanner.nextLine();
            if (!(validationService.validateBalance(balance))) {
                System.out.println("Saldo coin harus berupa angka");
                continue;
            }
            if (Integer.parseInt(balance) < 10000) {
                System.out.println("Top up Saldo coin minimal 10000");
                continue;
            }
            return balance;
        }
    }
}
