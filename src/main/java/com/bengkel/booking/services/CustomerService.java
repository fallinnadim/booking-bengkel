package com.bengkel.booking.services;

import java.util.List;

import com.bengkel.booking.models.BookingOrder;
import com.bengkel.booking.models.Customer;
import com.bengkel.booking.models.MemberCustomer;
import com.bengkel.booking.models.Vehicle;

public class CustomerService {
    public Customer getCustomerById(String customerId, List<Customer> listAllCustomers) {
        return listAllCustomers.stream()
                .filter(element -> element.getCustomerId().equals(customerId))
                .findFirst()
                .orElse(null);
    }

    public boolean checkMembership(Customer customer) {
        return customer instanceof MemberCustomer;
    }

    // fungsi dipastikan jalan jika member
    public double topUpCoin(Customer customer, String balance) {
        // tidak perlu handle ClassCast Exception
        MemberCustomer memberCustomer = (MemberCustomer) customer;
        double balanceValue = Double.parseDouble(balance); // tidak perlu validasi sudah dihandle
        memberCustomer.setSaldoCoin(memberCustomer.getSaldoCoin() + balanceValue);
        return memberCustomer.getSaldoCoin();
    }

    public Vehicle getCustomerVehicleByVehicleId(Customer customer, String vehicleId) {
        return customer.getVehicles()
                .stream()
                .filter(element -> element.getVehiclesId().equals(vehicleId))
                .findFirst()
                .orElse(null);
    }

    public void updateCustomerBalance(Customer customer, BookingOrder newBookingOrder) {
        MemberCustomer memberCustomer = (MemberCustomer) customer;
        double currentBalance = memberCustomer.getSaldoCoin();
        memberCustomer.setSaldoCoin(currentBalance - newBookingOrder.getTotalPayment());
    }
}
