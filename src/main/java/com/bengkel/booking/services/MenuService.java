package com.bengkel.booking.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.bengkel.booking.models.BookingOrder;
import com.bengkel.booking.models.Customer;
import com.bengkel.booking.models.ItemService;
import com.bengkel.booking.models.Vehicle;
import com.bengkel.booking.repositories.CustomerRepository;
import com.bengkel.booking.repositories.ItemServiceRepository;

public class MenuService {
    private static final List<Customer> listAllCustomers = CustomerRepository.getAllCustomer();
    private static final List<ItemService> listAllItemService = ItemServiceRepository.getAllItemService();
    private static final List<BookingOrder> listAllBookingOrders = new ArrayList<>();
    private static final Scanner input = new Scanner(System.in);

    public static void run() {
        PrintService printService = new PrintService();
        ValidationService validationService = new ValidationService();
        String[] listMenu = {"Login", "Exit"};
        int menuChoice = 0;
        boolean isLooping = true;
        while (isLooping) {
            printService.printMenu(listMenu, "Aplikasi Booking Bengkel");
            menuChoice = validationService.validasiNumberWithRange("Masukan Pilihan Menu:", "Input Harus Berupa Angka!",
                    "^[0-9]+$", listMenu.length - 1, 0);
            System.out.println(menuChoice);
            switch (menuChoice) {
                case 1:
                    loginMenu();
                    break;
                case 0:
                    System.out.println("Exit");
                    isLooping = false;
                    break;
            }
        }
    }

    public static void loginMenu() {
        ValidationService validationService = new ValidationService();
        PrintService printService = new PrintService();
        CustomerService customerService = new CustomerService();
        LoginService loginService = new LoginService(customerService);
        InputService inputService = new InputService(input, validationService);

        int errCount = 0;
        while (true) {
            if (errCount == 3) {
                printService.printErrorTooManyLoginAttempt();
                System.exit(0);
            }
            String customerId = inputService.getInputCustomerId();
            String password = inputService.getInputPassword();

            Customer customer = loginService.checkCustomerId(customerId, listAllCustomers);
            if (customer == null) {
                printService.printErrorCustomerIdNotFound();
                errCount++;
                continue;
            }
            boolean isCorrectPassword = loginService.checkPassword(customer, password);
            if (!isCorrectPassword) {
                printService.printErrorIncorrectPassword();
                errCount++;
                continue;
            }
            printService.printSuccessLogin();
            mainMenu(customer);
            break;
        }
    }

    public static void mainMenu(Customer customer) {
        PrintService printService = new PrintService();
        CustomerService customerService = new CustomerService();
        BengkelService bengkelService = new BengkelService(customerService);
        ValidationService validationService = new ValidationService(bengkelService);
        InputService inputService = new InputService(input, validationService);

        String[] listMenu = {"Informasi Customer", "Booking Bengkel", "Top Up Bengkel Coin", "Informasi Booking",
                "Logout"};
        int menuChoice = 0;
        boolean isLooping = true;
        while (isLooping) {
            printService.printMenu(listMenu, "Booking Bengkel Menu");
            menuChoice = validationService.validasiNumberWithRange("Masukan Pilihan Menu:", "Input Harus Berupa Angka!",
                    "^[0-9]+$", listMenu.length - 1, 0);
            System.out.println(menuChoice);
            switch (menuChoice) {
                case 1:
                    // panggil fitur Informasi Customer
                    printService.printCustomer(customer);
                    break;
                case 2:
                    // panggil fitur Booking Bengkel
                    printService.printVechicle(customer.getVehicles());
                    boolean validateVehileId = true;
                    String vehicleId = "";
                    Vehicle vehicle = null;
                    while (validateVehileId) {
                        vehicleId = inputService.getInputVehicleId();
                        vehicle = validationService.validasiVehicle(customer, vehicleId);
                        if (vehicle == null) {
                            printService.printErrorVehicleIdNotFound();
                            continue;
                        }
                        validateVehileId = false;
                    }

                    List<ItemService> availableServices = bengkelService.getAllServicesByVehicleType(listAllItemService,
                            vehicle.getVehicleType());
                    printService.printAllServices(availableServices);

                    List<ItemService> services = inputService.getInputListService(customer, availableServices);
                    boolean isMembership = customerService.checkMembership(customer);

                    boolean validateInputPaymentMethod = true;
                    String paymentMethod = "";
                    while (validateInputPaymentMethod) {
                        paymentMethod = inputService.getInputPaymentMethod();
                        boolean isRightPaymentType = validationService.validatePaymentType(paymentMethod);
                        if (!isRightPaymentType) {
                            printService.printErrorInvalidPaymentType();
                            continue;
                        }
                        boolean isRightPaymentMethod = validationService.validatePaymentMethod(paymentMethod, isMembership);
                        if (!isRightPaymentMethod) {
                            printService.printErrorInvalidPaymentMethod();
                            continue;
                        }
                        validateInputPaymentMethod = false;
                    }

                    boolean isEnoughBalance = validationService.validateEnoughBalance(customer, paymentMethod, services);
                    if (!isEnoughBalance) {
                        printService.printErrorInsufficientBalance();
                        break;
                    }
                    bengkelService.createBooking(listAllBookingOrders, customer, services, paymentMethod);
                    break;
                case 3:
                    // panggil fitur Top Up Saldo Coin
                    boolean isMembership2 = customerService.checkMembership(customer);
                    if (!isMembership2) {
                        printService.printErrorNotMembership();
                        break;
                    }
                    System.out.println("Top Up Saldo Coin\n");
                    String balance = inputService.getTopUpInput();
                    int newBalance = (int) customerService.topUpCoin(customer, balance);
                    printService.printSuccessTopUp(customer, newBalance);
                    break;
                case 4:
                    // panggil fitur Informasi Booking Order
                    printService.printAllBookingOrder(
                            bengkelService.getAllBookingOrderByCustomerId(listAllBookingOrders, customer));
                    break;
                case 0:
                    printService.printSuccessLogout();
                    isLooping = false;
                    break;
            }
        }
    }

}
