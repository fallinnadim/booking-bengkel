package com.bengkel.booking.services;

import java.util.List;
import java.util.Scanner;

import com.bengkel.booking.models.BookingOrder;
import com.bengkel.booking.models.Customer;
import com.bengkel.booking.models.ItemService;
import com.bengkel.booking.models.MemberCustomer;
import com.bengkel.booking.models.Vehicle;

public class ValidationService {
    private BengkelService bengkelService;

    public ValidationService() {
    }

    public ValidationService(BengkelService bengkelService) {
        this.bengkelService = bengkelService;
    }

    public String validasiInput(String question, String errorMessage, String regex) {
        Scanner input = new Scanner(System.in);
        String result = "";
        boolean isLooping = true;
        while (isLooping) {
            System.out.print(question);
            result = input.nextLine();
            if (result.matches(regex)) {
                isLooping = false;
            } else {
                System.out.println(errorMessage);
            }
        }
        return result;
    }

    public int validasiNumberWithRange(String question, String errorMessage, String regex, int max, int min) {
        int result = 0;
        boolean isLooping = true;
        while (isLooping) {
            result = Integer.parseInt(validasiInput(question, errorMessage, regex));
            if (result >= min && result <= max) {
                isLooping = false;
            } else {
                System.out.println("Pilihan angka " + min + " s.d " + max);
            }
        }
        return result;
    }

    public boolean validateBalance(String balance) {
        try {
            Double.valueOf(balance);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public Vehicle validasiVehicle(Customer customer, String vehicleId) {
        return bengkelService.getCustomerService().getCustomerVehicleByVehicleId(customer, vehicleId);

    }

    public boolean validatePaymentType(String inputPaymentMethod) {
        return inputPaymentMethod.equals("Saldo Coin") || inputPaymentMethod.equals("Cash");
    }

    public boolean validatePaymentMethod(String inputPaymentMethod, boolean isMembership) {
        return !inputPaymentMethod.equals("Saldo Coin") || isMembership;
    }

    public ItemService validasiItemService(Scanner input, List<ItemService> listAllItemService) {
        while (true) {
            System.out.println("Silahkan Masukan Service id:");
            String serviceId = input.nextLine();
            ItemService itemService = bengkelService.getServiceById(serviceId, listAllItemService);
            if (itemService == null) {
                System.out.println("Service id tidak ditemukan\n");
                continue;
            }
            return itemService;
        }
    }

    public boolean validasiServiceQuota(List<ItemService> services, Customer customer) {
        return services.size() == customer.getMaxNumberOfService();
    }

    public boolean validateEnoughBalance(Customer customer, String paymentMethod, List<ItemService> services) {
        if (!(customer instanceof MemberCustomer)) { // kalau non member proses selanjutnya
            return true;
        }
        if (paymentMethod.equals("Cash")) { // kalau cash proses selanjutnya
            return true;
        }
        // temp record untuk hitung harga (setelah diskon)
        BookingOrder tempBookingOrder = bengkelService.createBookingRecord(customer, services, paymentMethod);
        tempBookingOrder.calculatePayment(); // hitung harga
        return ((MemberCustomer) customer).getSaldoCoin() >= tempBookingOrder.getTotalPayment();
    }
}
