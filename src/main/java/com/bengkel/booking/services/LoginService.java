package com.bengkel.booking.services;

import java.util.List;

import com.bengkel.booking.models.Customer;

public class LoginService {
    private final CustomerService customerService;

    public LoginService(CustomerService customerService) {
        this.customerService = customerService;
    }

    public Customer checkCustomerId(String customerId, List<Customer> listAllCustomers) {
        return customerService.getCustomerById(customerId, listAllCustomers);
    }

    public boolean checkPassword(Customer found, String password) {
        return found.getPassword().equals(password);
    }


}
