package com.bengkel.booking.services;

import java.util.List;

import com.bengkel.booking.models.BookingOrder;
import com.bengkel.booking.models.Customer;
import com.bengkel.booking.models.ItemService;
import com.bengkel.booking.models.MemberCustomer;
import lombok.Getter;

@Getter
public class BengkelService {
	private final CustomerService customerService;

	public BengkelService(CustomerService customerService) {
		this.customerService = customerService;
	}

	public void createBooking(List<BookingOrder> listAllBookingOrders, Customer customer,
							  List<ItemService> services, String paymentMethod) {
		// Get the total Service Price
		BookingOrder newBookingOrder = createBookingRecord(customer, services, paymentMethod);
		newBookingOrder.generateId(customer.getCustomerId());
		newBookingOrder.calculatePayment();
		listAllBookingOrders.add(newBookingOrder);
		if (customer instanceof MemberCustomer && paymentMethod.equals("Saldo Coin")) {
			customerService.updateCustomerBalance(customer, newBookingOrder);
		}
	}

	public BookingOrder createBookingRecord(Customer customer, List<ItemService> services, String paymentMethod) {
		double totalServicePrice = services.stream().mapToDouble(ItemService::getPrice).sum();
		return BookingOrder
				.builder()
				.customer(customer)
				.services(services)
				.paymentMethod(paymentMethod)
				.totalServicePrice(totalServicePrice)
				.build();
	}

	public List<ItemService> getAllServicesByVehicleType(List<ItemService> listAllItemService,
			String vehicleType) {
		return listAllItemService.stream()
				.filter(element -> element.getVehicleType().equals(vehicleType))
				.toList();
	}

	public ItemService getServiceById(String serviceId, List<ItemService> listAllItemService) {
		return listAllItemService.stream()
				.filter(element -> element.getServiceId().equals(serviceId))
				.findFirst()
				.orElse(null);
	}

	public List<BookingOrder> getAllBookingOrderByCustomerId(List<BookingOrder> listAllBookingOrders,
			Customer customer) {
		return listAllBookingOrders.stream()
				.filter(element -> element.getCustomer().getCustomerId().equals(customer.getCustomerId()))
				.toList();
	}

}
