package com.bengkel.booking.services;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

import com.bengkel.booking.models.BookingOrder;
import com.bengkel.booking.models.Car;
import com.bengkel.booking.models.Customer;
import com.bengkel.booking.models.ItemService;
import com.bengkel.booking.models.MemberCustomer;
import com.bengkel.booking.models.Vehicle;

public class PrintService {

    public void printMenu(String[] listMenu, String title) {
        String line = "+---------------------------------+";
        int number = 1;
        String formatTable = " %-2s. %-25s %n";

        System.out.printf("%-25s %n", title);
        System.out.println(line);

        for (String data : listMenu) {
            if (number < listMenu.length) {
                System.out.printf(formatTable, number, data);
            } else {
                System.out.printf(formatTable, 0, data);
            }
            number++;
        }
        System.out.println(line);
        System.out.println();
    }

    public void printVechicle(List<Vehicle> listVehicle) {
        String formatTable = "| %-2s | %-15s | %-10s | %-15s | %-15s | %-5s | %-15s |%n";
        String line = "+----+-----------------+------------+-----------------+-----------------+-------+-----------------+\n";
        System.out.format(line);
        System.out.format(formatTable, "No", "Vechicle Id", "Warna", "Brand", "Transmisi", "Tahun", "Tipe Kendaraan");
        System.out.format(line);
        AtomicInteger number = new AtomicInteger(0);
        listVehicle
                .forEach(element -> {
                    number.incrementAndGet();
                    AtomicReference<String> vehicleType = element instanceof Car
                            ? new AtomicReference<String>("Mobil")
                            : new AtomicReference<String>("Motor");
                    System.out.format(formatTable, number.get(), element.getVehiclesId(), element.getColor(),
                            element.getBrand(),
                            element.getTransmisionType(), element.getYearRelease(), vehicleType.get());
                });
        System.out.printf("%s\n\n", line);
    }

    public void printCustomer(Customer customer) {
        String status = "";
        if (customer instanceof MemberCustomer) {
            status = "Member";
            printBaseCustomer(customer, status);
            System.out.format("| %-17s | %-17s %n", "Saldo Koin", ((MemberCustomer) customer).getSaldoCoin());
            printVechicle(customer.getVehicles());
            return;
        }
        status = "Non Member";
        printBaseCustomer(customer, status);
        printVechicle(customer.getVehicles());

    }

    private void printBaseCustomer(Customer customer, String status) {
        System.out.println(status + "\n");
        System.out.println("Customer Profile\n");
        System.out.format("| %-17s | %-17s %n", "Customer Id", customer.getCustomerId());
        System.out.format("| %-17s | %-17s %n", "Nama", customer.getName());
        System.out.format("| %-17s | %-17s %n", "Customer Status", status);
        System.out.format("| %-17s | %-17s %n", "Alamat", customer.getAddress());
    }

    public void printErrorNotMembership() {
        System.out.println("Maaf fitur ini hanya untuk Member saja!");
    }

    public void printSuccessTopUp(Customer customer, int newBalance) {
        System.out.printf("Sukses top up coin pada Customer id %s , saldo saat ini %d \n", customer.getCustomerId(),
                newBalance);
    }

    public void printAllServices(List<ItemService> allServices) {
        String formatTable = "| %-2s | %-15s | %-17s | %-15s | %-15s |%n";
        String line = "+----+-----------------+------------+-----------------+-----------------+-------+-----------------+\n";
        System.out.format(line);
        System.out.format(formatTable, "No", "Service Id", "Nama Service", "Tipe Kendaraan", "Harga");
        System.out.format(line);
        AtomicInteger number = new AtomicInteger(0);
        allServices
                .forEach(element -> {
                    number.incrementAndGet();
                    System.out.format(formatTable, number.get(), element.getServiceId(), element.getServiceName(),
                            element.getVehicleType(), element.getPrice());
                });
        System.out.printf("%s\n\n", line);
    }

    public void printAllBookingOrder(List<BookingOrder> bookingOrders) {
        if (bookingOrders.isEmpty()) {
            System.out.println("+----+-----------------+DATA TIDAK DITEMUKAN+-------+-----------------+\n");
            return;
        }
        String formatTable = "| %-2s | %-27s | %-17s | %-17s | %-17s | %-17s | %-43s | %-17s |%n";
        String line = "+----+-----------------+------------+-----------------+-----------------+-------+-----------------+\n";
        System.out.format(line);
        System.out.format(formatTable, "No", "Booking Id", "Nama Customer", "Payment Method", "Total Service",
                "Total Payment", "List Service", "Booking Date");
        System.out.format(line);
        AtomicInteger number = new AtomicInteger(0);
        bookingOrders
                .forEach(element -> {
                    number.incrementAndGet();
                    System.out.format(formatTable, number.get(), element.getBookingId(),
                            element.getCustomer().getName(),
                            element.getPaymentMethod(), element.getTotalServicePrice(), element.getTotalPayment(),
                            printService(element.getServices()), element.getBookingDate());
                });
        System.out.printf("%s\n\n", line);

    }

    private String printService(List<ItemService> services) {
        return services
                .stream()
                .map(ItemService::getServiceName)
                .collect(Collectors.joining(","));
    }

    public void printErrorTooManyLoginAttempt() {
        System.out.println("Terlalu banyak kesalahan input\nKeluar program...\n");
    }

    public void printErrorCustomerIdNotFound() {
        System.out.println("Customer Id tidak ditemukan atau salah");
    }

    public void printErrorIncorrectPassword() {
        System.out.println("Password yang anda masukan salah");
    }

    public void printSuccessLogin() {
        System.out.println("Successfully logged in");
    }

    public void printErrorVehicleIdNotFound() {
        System.out.println("ID Kendaraan tidak ditemukan");
    }

    public void printErrorInvalidPaymentType() {
        System.out.println("payment method harus diisi Saldo Coin / Cash");
    }

    public void printErrorInvalidPaymentMethod() {
        System.out.println("hanya member yang bisa memilih metode saldo coin");
    }

    public void printErrorInsufficientBalance() {
        System.out.println("Saldo coin anda tidak cukup\nSilahkan ulang dari awal proses ini");
    }

    public void printSuccessLogout() {
        System.out.println("Logout\nBerhasil Logout\n");
    }
}
