package com.bengkel.booking.models;

import java.time.LocalDate;
import java.util.List;
import com.bengkel.booking.interfaces.IBengkelPayment;
import com.bengkel.booking.interfaces.IDGenerator;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class BookingOrder implements IBengkelPayment, IDGenerator {
	private static int counter = 1;
	private String bookingId;
	private Customer customer;
	private List<ItemService> services;
	private String paymentMethod;
	private double totalServicePrice;
	private double totalPayment;
	@Builder.Default
	private LocalDate bookingDate = LocalDate.now();

	@Override
	public void calculatePayment() {
		double discount = 0;
		discount = paymentMethod.equalsIgnoreCase("Saldo Coin")
				? getTotalServicePrice() * RATES_DISCOUNT_SALDO_COIN
				: getTotalServicePrice() * RATES_DISCOUNT_CASH;

		setTotalPayment(getTotalServicePrice() - discount);
	}

	@Override
	public void generateId(String customerId) {
		int i = BookingOrder.counter++;
		String bookingPrefix = "Book-";
		String customerPrefix = customerId.substring(0, 5);
		String customerSuffix = customerId.substring(4);
		String bookingSuffix = "";
		String id = String.valueOf(i);
		for (int j = 0; j < 3 - id.length(); j++) { // 3 digit
			bookingSuffix += "0";
		}
		bookingSuffix += id;
		String bookingId = bookingPrefix + customerPrefix + bookingSuffix + customerSuffix;
		setBookingId(bookingId);
	}


}
